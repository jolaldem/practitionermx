console.log ("Aqui funcionando con Nodemon");

var movimientosJSON = require('./movimientos_v2.json')
var express = require('express')
var bodyparse = require('body-parser')
var app = express()
app.use(bodyparse.json())
var requestJson = require('request-json')


app.get('/',function(req,res)
{
  res.send('Hola API');
});

app.get('/v1/movimientos',function(req,res)
{
  res.sendfile('movimientos_v1.json');
});

app.get('/v2/movimientos',function(req,res)
{
  res.sendfile('movimientos_v2.json');
});

app.get('/v2/movimientos/:id',function(req,res)
{
  //console.log(req);
  //res.send('Hemos recibido su peticion de consulta num:'+req.params.id);
  res.send(movimientosJSON [ req.params.id-1 ]);
  //console.log(req.params.id);
});

app.get('/v2/movimientosq',function(req,res)
{
  console.log(req.query);
  //res.send('Hemos recibido su peticion de consulta num:'+req.params.id);
  res.send("Recibido");
  //console.log(req.params.id);
});

app.get('/v2/movimientosp/:id',function(req,res)
{
  console.log(req.params);
  //res.send('Hemos recibido su peticion de consulta num:'+req.params.id);
  res.send("Recibido");
  //console.log(req.params.id);
});


app.post('/v2/movimientos',function(req,res)
{
  //console.log(req.headers);
  //if (req.headers [ 'authorization' ] != undefined) {
  var nuevo =req.body
  nuevo.id = movimientosJSON.length +1;
  movimientosJSON.push(nuevo);
  //movimientosJSON.push(req.body);
  res.send('Movimiento dado de alta');
  //}
  //else
  //  res.send('No estas autorizado');
});

app.put('/v2/movimientos/:id',function(req,res) {
  var cambios =req.body
  var actual = movimientosJSON [ req.params.id-1 ];
  if(cambios.importe != undefined)
    actual.importe = cambios.importe;
  if(cambios.ciudad != undefined)
    actual.ciudad = cambios.ciudad;
  res.send('Cambios realizados');
});

app.delete('/v2/movimientos/:id',function(req,res) {
  var actual = movimientosJSON [ req.params.id-1 ];
  movimientosJSON.push({
  "id": movimientosJSON.length+1,
  "importe": actual.importe * (-1),
  "ciudad": actual.ciudad,
  "concepto": "Negativo de" + req.params.id
});
  res.send('Movimiento anulado');
});


//USUARIOS
var usuariosJSON = require('./usuarios.json')
var jsonQuery = require('json-query')

app.get('/v1/usuarios/:id',function(req,res) {
  res.send(usuariosJSON [ req.params.id-1 ]);
});

app.post('/v1/usuarios/login',function(req,res) {
  var email = req.headers ['email'];
  var password = req.headers ['password'];
  var resultados = jsonQuery('[email=' +email+ ']', {data:usuariosJSON})
  if (resultados.value != null && resultados.value.password == password) {
    usuariosJSON[resultados.value.id-1].status = true;
     res.send('{"login":"OK"}')
   }
   else {
    res.send('{"login":"error"}')
  }
  });

app.post('/v1/usuarios/logout',function(req,res) {
  //console.log(req.query);
  var usuarioActual = usuariosJSON [ req.query.id-1 ];
  if (usuarioActual.status) {
    usuarioActual.status = false;
    res.send("Desconectando . . . . Logout OK");
  }
  else {
    res.send("Favor de hacer Login");
  }
});


//MLAB

var suurlMlabRaiz = "https://api.mlab.com/api/1/databases/techumx/collections"
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumxjao/collections"
var suapiKey = "apiKey=50c5ea68e4b0a97d668bc84a"
var apiKey = "apiKey=jsSFzJVEZL9GmrkKJBKuXYB0QQXyoumN"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

app.get('/v3', function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    var coleccionesUsuario = []
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes") {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else {
      res.send(err)
    }
  })
})

app.get('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.get('?q={"idusuario":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.post('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })
})

app.put('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
    res.send(body)
  })
})

//MLAB CUENTAS
/*app.get('/v5', function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    var coleccionesUsuario = []
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes") {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v5/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else {
      res.send(err)
    }
  })
})*/
app.get('/v5/cuentas', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v5/cuentas/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas")
  clienteMlab.get('?f={"mov":1}&q={"idcuenta":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v5/clientes', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/clientes?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v5/clientes/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/clientes")
  clienteMlab.get('?q={"idusuario":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

//movimientos
app.get('/v3/movimientos', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

/*app.get('/v3/movimientos/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")
  clienteMlab.get('?q={"idmovimiento":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})
*/

app.listen(3000);
console.log("Escuchando en el puerto 3000");
